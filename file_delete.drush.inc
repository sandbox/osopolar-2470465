<?php

/**
 * @file
 *   Drush commands to delete files.
 */

/**
 * Implementation of hook_drush_command().
 */
function file_delete_drush_command() {
  $items = array();

  $items['file-delete-unused'] = array(
    'description' => "Deletes files that aren't used anymore.",
    'options' => array(
      'all' => 'Delete all unused files without individual confirmation.',
      'order-by-filesize' => 'Sort files by file-size descending, useful if you need to confirm each file deletion and just want to remove the larger files.'
    ),
    'aliases' => array('fdu'),
    'callback' => 'file_delete_unused',
  );

  return $items;
}

/**
 * Command callback for strongarm_revert.
 */
function file_delete_unused($type = NULL) {
  $all = FALSE;
  if (drush_get_option('all')) {
    $all = TRUE;
  }

  $query = db_select('file_managed', 'f')
            ->fields('f', array('fid'))
            ->isNull('fu.count');
  $query->leftJoin('file_usage', 'fu', 'f.fid=fu.fid');
  $or = db_or();
  $or->condition('fu.count', 0);
  $or->isNull('fu.count');
  $query->condition($or);

  if (drush_get_option('order-by-filesize')) {
    $query->orderBy("filesize", "DESC");
  }

  $total = $query->countQuery()->execute()->fetchField();;

  if (!$total) {
    drush_log(dt('No unused files found.'), 'ok');
    return TRUE;
  }

  drush_print(dt('This command deletes files that seems to be unused, because there is no usage registered in the database (table "files_usage").'));
  drush_print(dt('Be careful, because some modules might have uploaded files without proper database registration.'));
  drush_print(dt("You should only delete these files if you are sure that they aren't used anymore. The deletion of files could not be undone.") . "\n");

  // Ask for confirmation if all unused files should be deleted.
  if ($all && !drush_confirm(dt('!total unused files found. Are you sure to remove them?', array('!total' => $total)))) {
    return drush_user_abort();
  }

  $result = $query->execute();
  $count = 0;
  while($fid = $result->fetchField()) {
    $count++;
    $file = file_load($fid);
    // Use option "--all" to delete all unused files without
    // individual confirmation.
    if (!$all && !drush_confirm(dt('[File !count of !total:] Do you want to delete the unused file @filename?',
        array('!count' => $count, '!total' => $total, '@filename' => $file->uri)))) {
      continue;
    }

    file_delete($file);
  }
}
